import { getCustomRepository } from 'typeorm';
import Product from '../models/Product';
import ProductsRepository from '../repositories/ProductRepository';

// import ProductsRepository from '../repositories/ProductsRepository';

interface CreateProductsDTO {
  code: string;
  type: string;
  buy_value: number;
  sale_value: number;
  amount: number;
}

class CreateProducts {
  public async execute({
    code,
    type,
    buy_value,
    sale_value,
    amount,
  }: CreateProductsDTO): Promise<Product> {
    const productsRepository = getCustomRepository(ProductsRepository);

    const findCodeInProducts = await productsRepository.findByCode(code);

    if (findCodeInProducts) {
      throw Error('Code already exist');
    }

    if (buy_value <= 0 || sale_value <= 0 || amount <= 0) {
      throw Error('Invalid value');
    }

    const product = productsRepository.create({
      code,
      type,
      buy_value,
      sale_value,
      amount,
    });

    await productsRepository.save(product);

    // delete product.id;

    return product;
  }
}

export default CreateProducts;
