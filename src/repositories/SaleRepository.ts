import { EntityRepository, Repository, getConnection } from 'typeorm';

import Sale from '../models/Sale';

interface Balance {
  pending: number;
  paid: number;
  total: number;
}

@EntityRepository(Sale)
class SaleRepository extends Repository<Sale> {
  public async getSale(id: string): Promise<string[]> {
    const getSale = await getConnection()
      .createQueryBuilder()
      .select(['total_value'])
      .from(Sale, 'sale')
      .where('code = :code', { code: id })
      .getRawAndEntities();

    return getSale.raw;
  }

  public async getBalance(): Promise<Balance> {
    const transactions = await this.find();
    const { pending, paid } = transactions.reduce(
      (accumulator: Balance, transaction: Sale) => {
        switch (transaction.status) {
          case 'pending':
            accumulator.pending += Number(transaction.total_value);
            break;
          case 'paid':
            accumulator.paid += Number(transaction.total_value);
            break;
          default:
            break;
        }
        return accumulator;
      },
      {
        pending: 0,
        paid: 0,
        total: 0,
      },
    );
    const total = paid - pending;

    return { paid, pending, total };
  }
}

export default SaleRepository;
