import { EntityRepository, getConnection, Repository } from 'typeorm';
import Product from '../models/Product';

interface AmountSubtract {
  id_product: string;
  amountNow: number;
  sale_unity: number;
}

interface AmountUpdate {
  id: string;
  amount: number;
}

@EntityRepository(Product)
class ProductsRepository extends Repository<Product> {
  public async findByCode(code: string): Promise<Product | null> {
    const findCode = await this.findOne({
      where: { code },
    });

    return findCode || null;
  }

  public async getAmount(id_product: string): Promise<number> {
    const getAmountNow = await getConnection()
      .createQueryBuilder()
      .select(['amount'])
      .from(Product, 'products')
      .where('id = :id', { id: id_product })
      .getRawOne();

    return getAmountNow.amount;
  }

  public async getSale(id_product: string): Promise<number> {
    const getSaleValue = await getConnection()
      .createQueryBuilder()
      .select(['sale_value'])
      .from(Product, 'products')
      .where('id = :id', { id: id_product })
      .getRawOne();

    return getSaleValue.sale_value;
  }

  public async subtractAmount({
    id_product,
    amountNow,
    sale_unity,
  }: AmountSubtract): Promise<void> {
    const subtract = amountNow - sale_unity;

    await getConnection()
      .createQueryBuilder()
      .update(Product)
      .set({ amount: subtract })
      .where('id = :id', { id: id_product })
      .execute();
  }

  public async addAmount({ id, amount }: AmountUpdate): Promise<void> {
    const amountnow = await this.getAmount(id);

    const total = amountnow + amount;

    await getConnection()
      .createQueryBuilder()
      .update(Product)
      .set({ amount: total })
      .where('id = :id', { id })
      .execute();
  }
}

export default ProductsRepository;
