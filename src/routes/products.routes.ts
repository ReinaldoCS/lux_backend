import { Router } from 'express';
import { getCustomRepository } from 'typeorm';
import CreateProducts from '../services/CreateProducts';
import ProductRepository from '../repositories/ProductRepository';

const productsRoutes = Router();

productsRoutes.get('/', async (require, response) => {
  const productsRepository = getCustomRepository(ProductRepository);
  const products = await productsRepository.find();

  response.json({ products });
});

productsRoutes.post('/', async (require, response) => {
  try {
    const { code, type, buy_value, sale_value, amount } = require.body;

    const createProducts = new CreateProducts();

    const product = await createProducts.execute({
      code,
      type,
      buy_value,
      sale_value,
      amount,
    });

    return response.json(product);
  } catch (err) {
    return response.status(400).json({ error: err.message });
  }
});

productsRoutes.put('/', async (require, response) => {
  // const { id } = require.query;
  const { id, amount } = require.body;

  const productRepository = new ProductRepository();

  await productRepository.addAmount({
    id,
    amount,
  });

  response.json({ id, amount });
});

export default productsRoutes;
