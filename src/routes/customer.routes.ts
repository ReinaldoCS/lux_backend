import { Router } from 'express';
import { getCustomRepository } from 'typeorm';
import CustomerRepository from '../repositories/CustomerRepository';
import CreateCustomer from '../services/CreateCustomer';

const customerRoutes = Router();

customerRoutes.get('/', async (request, response) => {
  const customerRepository = getCustomRepository(CustomerRepository);

  const customers = await customerRepository.find();

  return response.json(customers);
});

customerRoutes.post('/', async (request, response) => {
  try {
    const { name, phone_number } = request.body;

    const createCustomer = new CreateCustomer();

    const customer = await createCustomer.execute({
      name,
      phone_number,
    });

    return response.json(customer);
  } catch (err) {
    return response.status(400).json({ message: err.message });
  }
});

export default customerRoutes;
