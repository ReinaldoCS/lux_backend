import { getCustomRepository } from 'typeorm';
import Sale from '../models/Sale';
import SaleRepository from '../repositories/SaleRepository';

interface CreateSaleDTO {
  code: string;
  id_customer: string;
  id_product: string;
  amountNow: number;
  sale_unity: number;
  productSaleValue: number;
  status: 'pending' | 'paid';
}

class CreateSale {
  public async execute({
    code,
    id_customer,
    id_product,
    amountNow,
    sale_unity,
    productSaleValue,
    status,
  }: CreateSaleDTO): Promise<Sale> {
    // const customerRepository = getCustomRepository(C);
    const saleRepository = getCustomRepository(SaleRepository);

    if (status !== 'pending' && status !== 'paid') {
      throw new Error('invalid status!');
    }

    if (amountNow - sale_unity < 0) {
      throw Error(`indisposable amount! Amount now: ${amountNow}`);
    }

    const total_value = productSaleValue * sale_unity;

    const sale = saleRepository.create({
      code,
      id_customer,
      id_product,
      sale_unity,
      total_value,
      status,
    });

    await saleRepository.save(sale);

    // delete sale.id;
    // delete sale.id_customer;
    // delete sale.id_product;

    return sale;
  }
}

export default CreateSale;
