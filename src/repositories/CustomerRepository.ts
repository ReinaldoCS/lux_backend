import { EntityRepository, getConnection, Repository } from 'typeorm';
import Customer from '../models/Customer';

interface UpdateOrder {
  id_customer: string;
  sale_unity: number;
}

@EntityRepository(Customer)
class CustomerRepository extends Repository<Customer> {
  public async findByName(name: string): Promise<Customer | null> {
    const findName = await this.findOne({
      where: { name },
    });

    return findName || null;
  }

  public async findById(id: string): Promise<Customer> {
    const findCustomer = await this.findOne({
      where: { id },
    });

    return findCustomer;
  }

  public async findOrderById(id: string): Promise<number> {
    const getOrder = await getConnection()
      .createQueryBuilder()
      .select(['order_number'])
      .from(Customer, 'customer')
      .where('id = :id', { id })
      .getRawOne();

    return getOrder.order_number;
  }

  public async updateOrderNumber({
    id_customer,
    sale_unity,
  }: UpdateOrder): Promise<void> {
    // const customer = await this.findById(id_customer);
    const orderNow = await this.findOrderById(id_customer);

    const total = orderNow + sale_unity;

    await getConnection()
      .createQueryBuilder()
      .update(Customer)
      .set({ order_number: total })
      .where('id = :id', { id: id_customer })
      .execute();
  }
}

export default CustomerRepository;
