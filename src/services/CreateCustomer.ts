import { getCustomRepository } from 'typeorm';
import Customer from '../models/Customer';
import CustomerRepository from '../repositories/CustomerRepository';

interface CustomerDTO {
  name: string;
  phone_number: string;
}

class CreateCustomer {
  public async execute({ name, phone_number }: CustomerDTO): Promise<Customer> {
    const customerRepository = getCustomRepository(CustomerRepository);

    const findCustomerName = await customerRepository.findByName(name);

    if (findCustomerName) {
      throw Error('Customer already exist');
    }

    const customer = customerRepository.create({
      name,
      phone_number,
    });

    await customerRepository.save(customer);

    // delete customer.id;

    return customer;
  }
}

export default CreateCustomer;
