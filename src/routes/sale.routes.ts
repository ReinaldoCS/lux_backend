import { Router } from 'express';
import { getCustomRepository } from 'typeorm';

import CreateSale from '../services/CreateSale';
import SaleRepository from '../repositories/SaleRepository';
import ProductsRepository from '../repositories/ProductRepository';
import CustomerRepository from '../repositories/CustomerRepository';

const saleRoutes = Router();

saleRoutes.get('/', async (request, response) => {
  const saleRepository = getCustomRepository(SaleRepository);

  const sales = await saleRepository.find();

  const balance = await saleRepository.getBalance();

  return response.json({ sales, balance });
});

saleRoutes.post('/:code', async (request, response) => {
  try {
    const { code } = request.params;

    const { id_customer, id_product, sale_unity, status } = request.body;

    const createSale = new CreateSale();
    const productRepository = new ProductsRepository();
    const customerRepository = new CustomerRepository();

    const amountNow = await productRepository.getAmount(id_product);
    const productSaleValue = await productRepository.getSale(id_product);

    const sale = await createSale.execute({
      code,
      id_customer,
      id_product,
      amountNow,
      sale_unity,
      productSaleValue,
      status,
    });

    await productRepository.subtractAmount({
      id_product,
      amountNow,
      sale_unity,
    });

    await customerRepository.updateOrderNumber({ id_customer, sale_unity });

    return response.json(sale);
  } catch (err) {
    return response.status(400).json({ message: err.message });
  }
});

saleRoutes.get('/test/:id', async (request, response) => {
  try {
    // const { id } = request.params;

    // const saleRepository = new SaleRepository();
    const saleRepository = getCustomRepository(SaleRepository);

    const sales = await saleRepository.find();

    return response.json(sales);
  } catch (err) {
    return response.status(400).json({ message: err.message });
  }
});

export default saleRoutes;
