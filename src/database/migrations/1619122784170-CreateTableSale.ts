import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export default class CreateTableSale1619122784170
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'sale',
        columns: [
          {
            name: 'id',
            type: 'varchar',
            isPrimary: true,
            generationStrategy: 'uuid',
            default: 'uuid_generate_v4()',
          },
          {
            name: 'code',
            type: 'varchar',
          },
          {
            name: 'id_customer',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'id_product',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'sale_unity',
            type: 'int',
          },
          {
            name: 'total_value',
            type: 'numeric',
            precision: 5,
            scale: 2,
          },
          {
            name: 'status',
            type: 'varchar',
          },
          {
            name: 'create_at',
            type: 'timestamp',
            default: 'now()',
          },
          {
            name: 'update_at',
            type: 'timestamp',
            default: 'now()',
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'sale',
      new TableForeignKey({
        name: 'idCustomerInSale',
        columnNames: ['id_customer'],
        referencedTableName: 'customer',
        referencedColumnNames: ['id'],
        onDelete: 'SET NULL',
        onUpdate: 'CASCADE',
      }),
    );

    await queryRunner.createForeignKey(
      'sale',
      new TableForeignKey({
        name: 'idProductInSale',
        columnNames: ['id_product'],
        referencedTableName: 'products',
        referencedColumnNames: ['id'],
        onDelete: 'SET NULL',
        onUpdate: 'CASCADE',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('sale', 'idProductInSale');

    await queryRunner.dropForeignKey('sale', 'idCustomerInSale');

    await queryRunner.dropTable('sale');
  }
}
