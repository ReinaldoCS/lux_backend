import { Router } from 'express';
import customerRoutes from './customer.routes';
import productsRoutes from './products.routes';
import saleRoutes from './sale.routes';

const routes = Router();

routes.use('/products', productsRoutes);
routes.use('/customer', customerRoutes);
routes.use('/sale', saleRoutes);

export default routes;
